#include <iostream>
#include <ctime>
using namespace std;

void InsertionSort(int A[], int N)
{
	int I, key, J;
	for (I = 1; I <= (N - 1); I++)
	{
		key = A[I];
		J = I - 1;
		while (J >= 0 && A[J] > key)
		{
			A[J + 1] = A[J];
			J--;
		}
		A[J + 1] = key;
	}
}

int main()
{
	int * A; //using a pointer to create a dynamic array instead of static array
	//int A[1000];
	int N;
	cout << "How much data do you want to sort: ";
	cin >> N;
	A = new int[N];//using a dynamic array
	int time1 = clock();
	int time2 = clock();
	float cputime = float(time2 - time1) / CLOCKS_PER_SEC;
	int i;

	for (i = 0; i <= N - 1; i++)
	{
		A[i] = rand();
	}
	InsertionSort(A, N); 

	for (i = 0; i <= N - 1; i++)
	{
		cout << A[i] << "\t";
		if (i % 10 == 0) cout << endl;
	}
	return 0;

}