# Sorting Algorithms Assignment #

### **About:** ###

This repo represents the source codes for a HW assignment the required me to write a program that takes input from the user or reads data based on random integers to call each of the following functions: 

- ​Merge Sort 
- Selection Sort
- ​Insertion Sort
- Quick Sort (including Partition) 

### **IDE:**
CodeRunner

### **Input:** ###
Random Integers

### **Language:** ###
Java and C++